# EPEL7 support: default Python is Python 2, and Python2 packages prefix
# is unversioned.
%if 0%{?rhel} && 0%{?rhel} <= 7
%global py2_namespace python
%global default_pyver 2
%else
%global py2_namespace python2
%global default_pyver 3
%endif

%global pkgname mailman-client
%global srcname mailmanclient
%global summary Python bindings for the Mailman 3 REST API
%global _description \
The ``mailman.client`` library provides official Python bindings for the \
GNU Mailman 3 REST API.

%bcond_with tests

Name:           python-%{pkgname}
Version:        3.2.0
Release:        %{?prerel:0.}1%{?dist}
Summary:        %{summary}

License:        LGPLv3
URL:            https://gitlab.com/mailman/mailmanclient
BuildArch:      noarch

Source0:        https://pypi.python.org/packages/source/m/%{srcname}/%{srcname}-%{version}%{?prerel:.dev0}.tar.gz

BuildRequires:  %{py2_namespace}-devel
BuildRequires:  %{py2_namespace}-setuptools
BuildRequires:  %{py2_namespace}-six
BuildRequires:  %{py2_namespace}-httplib2
# Test suite
%if %{with tests}
BuildRequires:  %{py2_namespace}-contextlib2
BuildRequires:  %{py2_namespace}-mock
%if 0%{?rhel} && 0%{?rhel} <= 7
BuildRequires:  pytest
BuildRequires:  pytest-vcr
%else
BuildRequires:  %{py2_namespace}-pytest
BuildRequires:  %{py2_namespace}-pytest-vcr
%endif
BuildRequires:  %{py2_namespace}-vcrpy
%endif

%{?python_provide:%python_provide %{py2_namespace}-%{pkgname}}
Requires:       %{py2_namespace}-setuptools
Requires:       %{py2_namespace}-six
Requires:       %{py2_namespace}-httplib2


%description %{_description}


%if %{py2_namespace} != python
%package -n     %{py2_namespace}-%{pkgname}
Summary:        %{summary}
%{?python_provide:%python_provide %{py2_namespace}-%{pkgname}}

Requires:       %{py2_namespace}-setuptools
Requires:       %{py2_namespace}-six
Requires:       %{py2_namespace}-httplib2

%description -n %{py2_namespace}-%{pkgname} %{_description}
%endif # py2_namespace != python


%package -n     python%{python3_pkgversion}-%{pkgname}
Summary:        %{summary}

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-six
BuildRequires:  python%{python3_pkgversion}-httplib2
# Test suite
%if %{with tests}
BuildRequires:  python%{python3_pkgversion}-contextlib2
BuildRequires:  python%{python3_pkgversion}-mock
BuildRequires:  python%{python3_pkgversion}-pytest
BuildRequires:  python%{python3_pkgversion}-vcrpy
%endif

%{?python_provide:%python_provide python%{python3_pkgversion}-%{pkgname}}
Requires:       python%{python3_pkgversion}-setuptools
Requires:       python%{python3_pkgversion}-six
Requires:       python%{python3_pkgversion}-httplib2

%description -n python%{python3_pkgversion}-%{pkgname} %{_description}


%if 0%{?with_python3_other}
%package -n python%{python3_other_pkgversion}-%{pkgname}
Summary:        %{summary}

BuildRequires:  python%{python3_other_pkgversion}-devel
BuildRequires:  python%{python3_other_pkgversion}-setuptools
BuildRequires:  python%{python3_other_pkgversion}-six
BuildRequires:  python%{python3_other_pkgversion}-httplib2
# Test suite
%if %{with tests}
BuildRequires:  python%{python3_other_pkgversion}-contextlib2
BuildRequires:  python%{python3_other_pkgversion}-mock
BuildRequires:  python%{python3_other_pkgversion}-pytest
BuildRequires:  python%{python3_other_pkgversion}-vcrpy
%endif

%{?python_provide:%python_provide python%{python3_other_pkgversion}-%{pkgname}}
Requires:       python%{python3_other_pkgversion}-setuptools
Requires:       python%{python3_other_pkgversion}-six
Requires:       python%{python3_other_pkgversion}-httplib2

%description -n python%{python3_other_pkgversion}-%{pkgname} %{_description}
%endif # with_python3_other


%prep
%autosetup -p1 -n %{srcname}-%{version}%{?prerel:.dev0}


%build
%py2_build
%py3_build
%if 0%{?with_python3_other}
%py3_other_build
%endif


%install
%py2_install
%py3_install

rm -f  %{buildroot}%{python2_sitelib}/%{srcname}/{README,NEWS}.rst
rm -rf %{buildroot}%{python2_sitelib}/%{srcname}/docs
rm -f  %{buildroot}%{python3_sitelib}/%{srcname}/{README,NEWS}.rst
rm -rf %{buildroot}%{python3_sitelib}/%{srcname}/docs

%if 0%{?with_python3_other}
%py3_other_install
rm -f  %{buildroot}%{python3_other_sitelib}/%{srcname}/{README,NEWS}.rst
rm -rf %{buildroot}%{python3_other_sitelib}/%{srcname}/docs
%endif


%check
%if %{with tests}
%{__python} -m pytest --vcr-record-mode=none
%{__python3} -m pytest --vcr-record-mode=none
%endif


%files
%license COPYING.LESSER
%doc src/mailmanclient/docs/*.rst
%{python_sitelib}/%{srcname}
%{python_sitelib}/%{srcname}-%{version}*-py?.?.egg-info

%files -n python%{python3_pkgversion}-%{pkgname}
%license COPYING.LESSER
%doc src/mailmanclient/docs/*.rst
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}*-py?.?.egg-info

%if 0%{?with_python3_other}
%files -n python%{python3_other_pkgversion}-%{pkgname}
%license COPYING.LESSER
%doc src/mailmanclient/docs/*.rst
%{python3_other_sitelib}/%{srcname}
%{python3_other_sitelib}/%{srcname}-%{version}*-py?.?.egg-info
%endif # with_python3_other


%changelog
* Fri Apr 12 2019 Marc Dequènes (Duck) <duck@redhat.com> - 3.2.0-1
- NUR
- remove patch fix-protect-unicode, integrated upstream
- remove patch hotfixes, not needed with recent Python 2/3

* Thu Nov 16 2017 Aurelien Bompard <abompard@fedoraproject.org> - 3.1.1-0.4
- git update to 9841f1c

* Mon May 29 2017 Aurelien Bompard <abompard@fedoraproject.org> - 3.1.1-0.1
- version 3.1.0.
- modernize spec file.

* Wed Apr 29 2015 Aurelien Bompard <abompard@fedoraproject.org> - 1.0.0-1
- version 1.0.0+rev70

* Thu Jun 19 2014 Aurelien Bompard <abompard@fedoraproject.org> - 1.0.0-0.5.b1
- new release

* Thu Mar 28 2013 Aurelien Bompard <abompard@fedoraproject.org> - 1.0.0a1
- Initial package.
