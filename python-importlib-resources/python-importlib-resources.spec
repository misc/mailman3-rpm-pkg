%global modname importlib_resources
%global pkgname importlib-resources

Name:             python-%{pkgname}
Version:          1.0.2
Release:          2%{?dist}
Summary:          importlib.resources backport

License:          MIT
URL:              https://pypi.io/project/%{pkgname}
Source0:          %pypi_source %{modname}
Patch0:           unittest_py2_failure_workaround.patch

BuildArch:        noarch

#BuildRequires:    python2-devel
#BuildRequires:    python2-setuptools
#BuildRequires:    python2-pytest
#BuildRequires:    python2-wheel
#BuildRequires:    python2-typing
#BuildRequires:    python2-pathlib2

BuildRequires:    python%{python3_pkgversion}-devel
BuildRequires:    python%{python3_pkgversion}-setuptools
BuildRequires:    python%{python3_pkgversion}-pytest
BuildRequires:    python%{python3_pkgversion}-wheel


%global _description\
importlib_resources is a backport of Python 3.7’s standard library\
importlib.resources module for Python 2.7, and 3.4 through 3.6.\
Users of Python 3.7 and beyond should use the standard library module,\
since for these versions, importlib_resources just delegates to that\
module.\
\
The key goal of this module is to replace parts of pkg_resources with\
a solution in Python’s stdlib that relies on well-defined APIs. This\
makes reading resources included in packages easier, with more stable\
and consistent semantics.

%description %_description

#%package -n python2-%{pkgname}
#Summary:          %summary
#
#%{?python_provide:%python_provide python2-%{pkgname}}
#
#%description -n python2-%{pkgname} %_description

%package -n python%{python3_pkgversion}-%{pkgname}
Summary:          %summary

%{?python_provide:%python_provide python%{python3_pkgversion}-%{pkgname}}

%description -n python%{python3_pkgversion}-%{pkgname} %_description

%prep
%autosetup -p1 -n %{modname}-%{version}

rm -rf %{py3dir}
cp -a . %{py3dir}

%build
#%{__python2} setup.py build

pushd %{py3dir}
%{__python3} setup.py build
popd


%install

install -d -m 0755 %{buildroot}%{_mandir}/man1

pushd %{py3dir}
%{__python3} setup.py install --skip-build --root=%{buildroot}
popd

#%{__python2} setup.py install -O1 --skip-build --root=%{buildroot}

%check
#%{__python2} setup.py test

%{__python3} setup.py test


#%files -n python2-%{pkgname}
#%doc README.rst LICENSE
#%{python2_sitelib}/%{modname}/
#%{python2_sitelib}/%{modname}-%{version}*

%files -n python%{python3_pkgversion}-%{pkgname}
%doc LICENSE README.rst
%{python3_sitelib}/%{modname}/
%{python3_sitelib}/%{modname}-%{version}-*


%changelog
* Fri Dec 20 2019 Marc Dequènes (Duck) <duck@redhat.com> - 1.0.2-2
- typing backport is not necessary for Python >= 3.5
- pathlib2 backport is not necessary for Python >= 3.5
- add patch to workaround unittest loading the Python 2 path of the code

* Wed Apr 10 2019 Marc Dequènes (Duck) <duck@redhat.com> - 1.0.2-1
- Initial release
- limit to Python 3 because of https://gitlab.com/python-devs/importlib_resources/issues/57
