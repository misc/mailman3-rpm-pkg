%global modname alembic

Name:             python-alembic
Version:          1.0.8
Release:          2%{?dist}
Summary:          Database migration tool for SQLAlchemy

License:          MIT
URL:              https://pypi.io/project/alembic
Source0:          %pypi_source alembic

BuildArch:        noarch

BuildRequires:    help2man
BuildRequires:    python2-devel
BuildRequires:    python2-setuptools
BuildRequires:    python2-mock
BuildRequires:    python2-dateutil
%if 0%{?fedora}
BuildRequires:    python2-editor
BuildRequires:    python2-mako
%else
BuildRequires:    python-editor
BuildRequires:    python-mako
%endif
BuildRequires:    python2-pytest

# See if we're building for python earlier than 2.7
%if 0%{?rhel} && 0%{?rhel} <= 6
BuildRequires:    python-sqlalchemy0.7 >= 0.7.4
BuildRequires:    python-argparse
BuildRequires:    python-nose1.1
%else
BuildRequires:    python2-nose
%if 0%{?fedora}
BuildRequires:    python2-sqlalchemy >= 0.7.4
%else
BuildRequires:    python-sqlalchemy >= 0.7.4
%endif
%endif

# Just for the tests
%if 0%{?fedora}
BuildRequires:    python2-mysql
BuildRequires:    python2-psycopg2
%else
BuildRequires:    MySQL-python
BuildRequires:    python-psycopg2
%endif

BuildRequires:    python%{python3_pkgversion}-devel
BuildRequires:    python2-tools
BuildRequires:    python%{python3_pkgversion}-sqlalchemy >= 0.7.4
BuildRequires:    python%{python3_pkgversion}-mako
BuildRequires:    python%{python3_pkgversion}-nose
BuildRequires:    python%{python3_pkgversion}-setuptools
BuildRequires:    python%{python3_pkgversion}-mock
BuildRequires:    python%{python3_pkgversion}-dateutil
BuildRequires:    python%{python3_pkgversion}-editor
BuildRequires:    python%{python3_pkgversion}-pytest


%global _description\
Alembic is a new database migrations tool, written by the author of\
SQLAlchemy.  A migrations tool offers the following functionality:\
\
* Can emit ALTER statements to a database in order to change the structure\
  of tables and other constructs.\
* Provides a system whereby "migration scripts" may be constructed; each script\
  indicates a particular series of steps that can "upgrade" a target database\
  to a new version, and optionally a series of steps that can "downgrade"\
  similarly, doing the same steps in reverse.\
* Allows the scripts to execute in some sequential manner.\
\
Documentation and status of Alembic is at http://readthedocs.org/docs/alembic/

%description %_description

%package -n python2-alembic
Summary:          %summary

# See if we're building for python earlier than 2.7
%if 0%{?rhel} && 0%{?rhel} <= 6
Requires:         python-sqlalchemy0.7 >= 0.7.4
Requires:         python-argparse
%else
Requires:         python2-sqlalchemy >= 0.9.0
%endif

Requires:         python2-editor
Requires:         python2-dateutil
Requires:         python2-setuptools
Requires:         python2-mako
%{?python_provide:%python_provide python2-alembic}

%description -n python2-alembic %_description

%package -n python%{python3_pkgversion}-alembic
Summary:          %summary

Requires:         python%{python3_pkgversion}-sqlalchemy >= 0.9.0
Requires:         python%{python3_pkgversion}-mako
Requires:         python%{python3_pkgversion}-setuptools
Requires:         python%{python3_pkgversion}-editor
Requires:         python%{python3_pkgversion}-dateutil
%{?python_provide:%python_provide python%{python3_pkgversion}-alembic}

%description -n python%{python3_pkgversion}-alembic %_description

%prep
%autosetup -p1 -n %{modname}-%{version}

rm -rf %{py3dir}
cp -a . %{py3dir}

# Make sure that epel/rhel picks up the correct version of sqlalchemy
%if 0%{?rhel} && 0%{?rhel} <= 6
awk 'NR==1{print "import __main__; __main__.__requires__ = __requires__ = [\"sqlalchemy>=0.6\", \"nose>=0.11\"]; import pkg_resources"}1' setup.py > setup.py.tmp
mv setup.py.tmp setup.py
%endif

%build
%{__python2} setup.py build

pushd %{py3dir}
%{__python3} setup.py build
popd

# Hack around setuptools so we can get access to help strings for help2man
# Credit for this goes to Toshio Kuratomi
%if 0%{?rhel} && 0%{?rhel} <= 6
%else
%{__mkdir_p} bin
echo 'python2 -c "import alembic.config; alembic.config.main()" $*' > bin/alembic
chmod 0755 bin/alembic
help2man --version-string %{version} --no-info -s 1 bin/alembic > python2-alembic.1
mv bin/alembic bin/python2-alembic
%endif

pushd %{py3dir}
%{__mkdir_p} bin
echo 'python3 -c "import alembic.config; alembic.config.main()" $*' > bin/alembic
chmod 0755 bin/alembic
help2man --version-string %{version} --no-info -s 1 bin/alembic > alembic.1
popd


%install

install -d -m 0755 %{buildroot}%{_mandir}/man1

pushd %{py3dir}
%{__python3} setup.py install --skip-build --root=%{buildroot}
mv %{buildroot}/%{_bindir}/%{modname} %{buildroot}/%{_bindir}/%{modname}-3
ln -s %{_bindir}/%{modname}-3 %{buildroot}/%{_bindir}/%{modname}-%{python3_version}
install -m 0644 alembic.1 %{buildroot}%{_mandir}/man1/alembic-3.1
ln -s %{_mandir}/man1/alembic-3.1 %{buildroot}%{_mandir}/man1/alembic-%{python3_version}.1
popd

%{__python2} setup.py install -O1 --skip-build --root=%{buildroot}
ln -s %{_bindir}/%{modname} %{buildroot}/%{_bindir}/%{modname}-2
ln -s %{_bindir}/%{modname} %{buildroot}/%{_bindir}/%{modname}-%{python2_version}
install -m 0644 python2-alembic.1 %{buildroot}%{_mandir}/man1/alembic.1
ln -s %{_mandir}/man1/alembic.1 %{buildroot}%{_mandir}/man1/alembic-2.1
ln -s %{_mandir}/man1/alembic.1 %{buildroot}%{_mandir}/man1/alembic-%{python2_version}.1

%if 0%{?rhel} && 0%{?rhel} <= 6
# Modify /usr/bin/alembic to require SQLAlchemy>=0.6
# Hacky but setuptools only creates this file after setup.py install is run :-(
# Root cause is that setuptools doesn't recurse the requirements when it processes
# the __requires__.  It waits until pkg_resources.require('MODULE') is called.
# Since that isn't done in the entrypoints script, we need to specify the dependency
# on a specific SQLAlchemy version explicitly.
sed -i -e "s|__requires__ = 'alembic==0.4.2'|__requires__ = ['alembic==0.4.2', 'SQLAlchemy>=0.6']|" %{buildroot}%{_bindir}/python2-%{modname}
%endif

%check
%{__python2} setup.py test

# skip on EL7, pytest is too old
%if 0%{?fedora}
pushd %{py3dir}
%{__python3} setup.py test
popd
%endif


%files -n python2-alembic
%doc README.rst LICENSE CHANGES docs
%{python2_sitelib}/%{modname}/
%{python2_sitelib}/%{modname}-%{version}*
%{_bindir}/%{modname}
%{_bindir}/%{modname}-2
%{_bindir}/%{modname}-%{python2_version}

%if 0%{?rhel} && 0%{?rhel} <= 6
%else
%{_mandir}/man1/alembic.1*
%{_mandir}/man1/alembic-2.1*
%{_mandir}/man1/alembic-%{python2_version}.1*
%endif

%files -n python%{python3_pkgversion}-%{modname}
%doc LICENSE README.rst CHANGES docs
%{python3_sitelib}/%{modname}/
%{python3_sitelib}/%{modname}-%{version}-*
%{_bindir}/%{modname}-3
%{_bindir}/%{modname}-%{python3_version}
%{_mandir}/man1/alembic-3.1*
%{_mandir}/man1/alembic-%{python3_version}.1*


%changelog
* Wed Apr 10 2019 Marc Dequènes (Duck) <duck@redhat.com> - 1.0.8-2
- Adaptations to build Python 3 on EPEL

* Thu Mar 28 2019 Randy Barlow <bowlofeggs@fedoraproject.org> - 1.0.8-1
- Update to 1.0.8 (#1685262).
- https://alembic.sqlalchemy.org/en/latest/changelog.html#change-1.0.8

* Tue Feb 05 2019 Alfredo Moralejo <amoralej@redhat.com> - 1.0.7-1
- Update to 1.0.7

* Sat Feb 02 2019 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Wed Jul 25 2018 Pierre-Yves Chibon <pingou@pingoured.fr> - 1.0.0-1
- Update to 1.0.0

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.9.7-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Tue Jun 19 2018 Miro Hrončok <mhroncok@redhat.com> - 0.9.7-6
- Rebuilt for Python 3.7

* Sun Jun 17 2018 Miro Hrončok <mhroncok@redhat.com> - 0.9.7-5
- Rebuilt for Python 3.7

* Wed Feb 21 2018 Iryna Shcherbina <ishcherb@redhat.com> - 0.9.7-4
- Update Python 2 dependency declarations to new packaging standards
  (See https://fedoraproject.org/wiki/FinalizingFedoraSwitchtoPython3)

* Thu Feb 08 2018 Randy Barlow <bowlofeggs@fedoraproject.org> - 0.9.7-3
- The python3-alembic package now provides only alembic-3 and alembic-3.y.
- The python2-alembic package now provides alembic, alembic-2, and alembic-2.y.

* Sat Jan 27 2018 Ralph Bean <rbean@redhat.com> - 0.9.7-2
- The python3-alembic package now provides the alembic executable.

* Thu Jan 18 2018 Ralph Bean <rbean@redhat.com> - 0.9.7-1
- new version
- New dependency on python-dateutil.
