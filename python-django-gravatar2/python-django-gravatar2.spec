# Created by pyp2rpm-1.0.0
%global pypi_name django-gravatar2

Name:           python-%{pypi_name}
Version:        1.0.6
Release:        3%{?dist}
Summary:        Essential Gravatar support for Django

License:        MIT
URL:            https://github.com/twaddington/django-gravatar
Source0:        https://pypi.python.org/packages/source/d/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python-devel

# renamed from django-browserid
Provides:       django-gravatar2 = %{version}-%{release}
Obsoletes:      django-gravatar2 < 1.0.6-2


%description
A lightweight django-gravatar app. Includes helper methods for interacting with
gravatars outside of template code.


%prep
%setup -q -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info


%build
%{__python} setup.py build


%install
%{__python} setup.py install --skip-build --root %{buildroot}


%files
%doc README.rst
%{python_sitelib}/django_gravatar
%{python_sitelib}/django_gravatar2-%{version}-py?.?.egg-info

%changelog
* Tue Apr 16 2019 Marc Dequènes (Duck) <duck@redhat.com> - 1.0.6-3
- use HTTPS in source URL

* Thu Dec 06 2012 Aurelien Bompard <abompard@fedoraproject.org> - 1.0.6-1
- Initial package.
