%global upstream xapian-haystack

Name:           python-django-haystack-xapian
Version:        2.0.0
Release:        2%{?dist}
Summary:        Xapian backend for Django-Haystack

License:        GPLv2
URL:            https://github.com/notanumber/xapian-haystack
# Build from the git clone with `python setup.py sdist`
Source0:        %{upstream}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python2-devel
#BuildRequires:  python-setuptools
#BuildRequires:  python-nose
#BuildRequires:  python-django-haystack
#BuildRequires:  xapian-bindings-python

Requires:       python-django-haystack
Requires:       xapian-bindings-python


%description
Xapian-haystack is a backend of Django-Haystack for the Xapian search engine.
Xapian-Haystack provides all the standard features of Haystack.


%prep
%setup -q -n %{upstream}-%{version}


%build
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}


%files
%doc LICENSE PKG-INFO README.rst AUTHORS
%{python_sitelib}/xapian_backend.py*
%{python_sitelib}/xapian_haystack-%{version}*.egg-info


%changelog
* Fri Mar 06 2015 Aurelien Bompard <abompard@fedoraproject.org> - 2.0.0-1
- initial package
