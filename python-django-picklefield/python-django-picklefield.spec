# EPEL7 support: default Python is Python 2, and Python2 packages prefix
# is unversioned.
%if 0%{?rhel} && 0%{?rhel} <= 7
%global py2_namespace python
%global default_pyver 2
# Until python3-django is packaged:
%global with_python3 0
%else
%global py2_namespace python2
%global default_pyver 3
%endif

%global pkgname django-picklefield
%global srcname picklefield
%global summary Pickled object field for Django
%global _description \
Django-picklefield provides an implementation of a pickled object field. \
Such fields can contain any picklable objects. \
\
The implementation is taken and adopted from Django snippet #1694 by \
Taavi Taijala, which is in turn based on Django snippet #513 by \
Oliver Beattie.


Name:       python-%{pkgname}
Version:    0.3.2
Release:    2%{?dist}
Summary:    %{summary}

Group:      Development/Libraries
License:    MIT
URL:        https://github.com/gintas/django-picklefield
Source0:    https://files.pythonhosted.org/packages/source/d/%{pkgname}/%{pkgname}-%{version}.tar.gz
# https://github.com/gintas/django-picklefield/issues/25
Source1:    test_settings.py
# source vanished
#Source1:    https://raw.githubusercontent.com/gintas/django-picklefield/master/test_settings.py

BuildArch:      noarch

BuildRequires:  %{py2_namespace}-devel
BuildRequires:  %{py2_namespace}-setuptools
BuildRequires:  %{py2_namespace}-django

Requires:       %{py2_namespace}-django

%description %{_description}


%if %{py2_namespace} != python
%package -n %{py2_namespace}-%{pkgname}
Summary:        %{summary}
Group:          Development/Libraries
%{?python_provide:%python_provide %{py2_namespace}-%{pkgname}}

Requires:       %{py2_namespace}-django

%description -n %{py2_namespace}-%{pkgname} %{_description}
%endif # py2_namespace != python


%if 0%{?with_python3}
%package -n python%{python3_pkgversion}-%{pkgname}
Summary:        %{summary}
Group:          Development/Libraries

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-django

Requires:       python%{python3_pkgversion}-django

%description -n python%{python3_pkgversion}-%{pkgname} %{_description}

%if 0%{?with_python3_other}
%package -n python%{python3_other_pkgversion}-%{pkgname}
Summary:        %{summary}
Group:          Development/Libraries

BuildRequires:  python%{python3_other_pkgversion}-devel
BuildRequires:  python%{python3_other_pkgversion}-setuptools
BuildRequires:  python%{python3_other_pkgversion}-django

Requires:       python%{python3_other_pkgversion}-django

%description -n python%{python3_other_pkgversion}-%{pkgname} %{_description}
%endif # with_python3_other
%endif # with_python3


%prep
%autosetup -n %{pkgname}-%{version}
cp -p %{SOURCE1} .


%build
%py2_build
%if 0%{?with_python3}
%py3_build
%if 0%{?with_python3_other}
%py3_other_build
%endif
%endif # with_python3


%install
%py2_install
%if 0%{?with_python3}
%py3_install
%if 0%{?with_python3_other}
%py3_other_install
%endif
%endif # with_python3


%check
export PYTHONPATH=src:.
%{__python2} %{_bindir}/django-admin test --settings test_settings picklefield
%if 0%{?with_python3}
%{__python3} %{_bindir}/django-admin test --settings test_settings picklefield
%if 0%{?with_python3_other}
%{__python3_other} %{_bindir}/django-admin test --settings test_settings picklefield
%endif
%endif # with_python3


%files
#%%license LICENSE
%doc README.rst
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/django_%{srcname}-*.egg-info


%if 0%{?with_python3}
%files -n python%{python3_pkgversion}-%{pkgname}
#%%license LICENSE
%doc README.rst
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/django_%{srcname}-*.egg-info

%if 0%{?with_python3_other}
%files -n python%{python3_other_pkgversion}-%{pkgname}
#%%license LICENSE
%doc README.rst
%{python3_other_sitelib}/%{srcname}
%{python3_other_sitelib}/django_%{srcname}-*.egg-info
%endif # with_python3_other

%endif # with_python3



%changelog
* Tue Apr 16 2019 Marc Dequènes (Duck) <duck@redhat.com> - 0.3.2-2
- remote source for test_settings.py vanished, embed it

* Tue Mar 14 2017 Aurelien Bompard <abompard@fedoraproject.org> - 0.3.2-1
- Initial package.
