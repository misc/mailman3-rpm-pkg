# EPEL7 support: default Python is Python 2, and Python2 packages prefix
# is unversioned.
%if 0%{?rhel} && 0%{?rhel} <= 7
%global py2_namespace python
%global default_pyver 2
# Until python3-django is packaged:
%global with_python3 0
%else
%global py2_namespace python2
%global default_pyver 3
%endif

%global pkgname django-q
%global srcname django_q
%global summary A multiprocessing distributed task queue for Django
%global _description \
Django Q is a native Django task queue, scheduler and worker application \
using Python multiprocessing. \
\
Features:  \
- Multiprocessing worker pools  \
- Asynchronous tasks  \
- Scheduled and repeated tasks  \
- Encrypted and compressed packages  \
- Failure and success database or cache  \
- Result hooks, groups and chains  \
- Django Admin integration  \
- PaaS compatible with multiple instances  \
- Multi cluster monitor  \
- Redis, Disque, IronMQ, SQS, MongoDB or ORM  \
- Rollbar support


Name:       python-%{pkgname}
Version:    0.7.18
Release:    1%{?dist}
Summary:    %{summary}

Group:      Development/Libraries
License:    MIT
URL:        https://django-q.readthedocs.io
Source0:    https://files.pythonhosted.org/packages/source/d/%{pkgname}/%{pkgname}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  %{py2_namespace}-devel
BuildRequires:  %{py2_namespace}-setuptools
BuildRequires:  %{py2_namespace}-django
BuildRequires:  %{py2_namespace}-django-picklefield
BuildRequires:  %{py2_namespace}-blessed
BuildRequires:  %{py2_namespace}-arrow
BuildRequires:  %{py2_namespace}-future
# Unit tests
%if 0%{?rhel} && 0%{?rhel} <= 7
# Pytest is too old here, run the tests with the usual Django command.
%else
BuildRequires:  %{py2_namespace}-pytest
BuildRequires:  %{py2_namespace}-pytest-django
%endif

Requires:       %{py2_namespace}-django >= 1.8
Requires:       %{py2_namespace}-django-picklefield
Requires:       %{py2_namespace}-blessed
Requires:       %{py2_namespace}-arrow
Requires:       %{py2_namespace}-future

%description %{_description}


%if %{py2_namespace} != python
%package -n %{py2_namespace}-%{pkgname}
Summary:        %{summary}
Group:          Development/Libraries
%{?python_provide:%python_provide %{py2_namespace}-%{pkgname}}

Requires:       %{py2_namespace}-django >= 1.8
Requires:       %{py2_namespace}-django-picklefield
Requires:       %{py2_namespace}-blessed
Requires:       %{py2_namespace}-arrow
Requires:       %{py2_namespace}-future

%description -n %{py2_namespace}-%{pkgname} %{_description}
%endif # py2_namespace != python


%if 0%{?with_python3}
%package -n python%{python3_pkgversion}-%{pkgname}
Summary:        %{summary}
Group:          Development/Libraries

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-django >= 1.8
BuildRequires:  python%{python3_pkgversion}-django-picklefield
BuildRequires:  python%{python3_pkgversion}-blessed
BuildRequires:  python%{python3_pkgversion}-arrow
BuildRequires:  python%{python3_pkgversion}-future
# Unit tests
BuildRequires:  python%{python3_pkgversion}-pytest
BuildRequires:  python%{python3_pkgversion}-pytest-django

Requires:       python%{python3_pkgversion}-django >= 1.8
Requires:       python%{python3_pkgversion}-django-picklefield
Requires:       python%{python3_pkgversion}-blessed
Requires:       python%{python3_pkgversion}-arrow
Requires:       python%{python3_pkgversion}-future

%description -n python%{python3_pkgversion}-%{pkgname} %{_description}

%if 0%{?with_python3_other}
%package -n python%{python3_other_pkgversion}-%{pkgname}
Summary:        %{summary}
Group:          Development/Libraries

BuildRequires:  python%{python3_other_pkgversion}-devel
BuildRequires:  python%{python3_other_pkgversion}-setuptools
BuildRequires:  python%{python3_other_pkgversion}-django >= 1.8
BuildRequires:  python%{python3_other_pkgversion}-django-picklefield
BuildRequires:  python%{python3_other_pkgversion}-blessed
BuildRequires:  python%{python3_other_pkgversion}-arrow
BuildRequires:  python%{python3_other_pkgversion}-future
# Unit tests
BuildRequires:  python%{python3_other_pkgversion}-pytest
BuildRequires:  python%{python3_other_pkgversion}-pytest-django

Requires:       python%{python3_other_pkgversion}-django >= 1.8
Requires:       python%{python3_other_pkgversion}-django-picklefield
Requires:       python%{python3_other_pkgversion}-blessed
Requires:       python%{python3_other_pkgversion}-arrow
Requires:       python%{python3_other_pkgversion}-future

%description -n python%{python3_other_pkgversion}-%{pkgname} %{_description}
%endif # with_python3_other
%endif # with_python3


%prep
%autosetup -n %{pkgname}-%{version}


%build
%py2_build
%if 0%{?with_python3}
%py3_build
%if 0%{?with_python3_other}
%py3_other_build
%endif
%endif # with_python3


%install
%py2_install
%if 0%{?with_python3}
%py3_install
%if 0%{?with_python3_other}
%py3_other_install
%endif
%endif # with_python3


%check
# The test suite isn't included in the tarball:
# https://github.com/Koed00/django-q/issues/226
exit 0
# When this bug is fixed, run the tests using the commands below.
%if 0%{?rhel} && 0%{?rhel} <= 7
%{__python2} %{_bindir}/django-admin test --pythonpath=. --settings django_q.tests.settings django_q
%else
%{__python2} setup.py test
%endif
%if 0%{?with_python3}
%{__python3} setup.py test
%if 0%{?with_python3_other}
%{__python3_other} setup.py test
%endif
%endif # with_python3


%files
%license LICENSE
%doc README.rst
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-*.egg-info


%if 0%{?with_python3}
%files -n python%{python3_pkgversion}-%{pkgname}
%license LICENSE
%doc README.rst
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-*.egg-info

%if 0%{?with_python3_other}
%files -n python%{python3_other_pkgversion}-%{pkgname}
%license LICENSE
%doc README.rst
%{python3_other_sitelib}/%{srcname}
%{python3_other_sitelib}/%{srcname}-*.egg-info
%endif # with_python3_other

%endif # with_python3



%changelog
* Mon Mar 13 2017 Aurelien Bompard <abompard@fedoraproject.org> - 0.7.18-1
- Initial package.
