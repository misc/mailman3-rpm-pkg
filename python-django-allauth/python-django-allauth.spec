%if 0%{?fedora} > 12 || 0%{?rhel} > 6
%global with_python3 1
%endif
# Until python3-django is packaged:
%global with_python3 0

%if 0%{?fedora}
%{!?python3_pkgversion: %global python3_pkgversion 3}
%else
%{!?python3_pkgversion: %global python3_pkgversion 34}
%endif

%global pkgname django-allauth

Name:           python-%{pkgname}
Version:        0.34.0
Release:        1%{?dist}
Summary:        Django app for local and social authentication
Group:          Development/Libraries
License:        MIT
URL:            http://www.intenct.nl/projects/django-allauth
Source0:        https://pypi.org/packages/source/d/%{pkgname}/%{pkgname}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python2-devel
BuildRequires:  python-sphinx
BuildRequires:  python-setuptools
BuildRequires:  python-django
BuildRequires:  python-mock >= 1.0.1
BuildRequires:  python-requests
BuildRequires:  python-requests-oauthlib
BuildRequires:  python-openid

# See setup.py, not requirements.txt
Requires:       python-django >= 1.8
Requires:       python-openid >= 2.2.5
Requires:       python-requests
Requires:       python-requests-oauthlib >= 0.3.0

%description
Integrated set of Django applications addressing authentication, registration,
account management as well as 3rd party (social) account authentication.


%if 0%{?with_doc}
%package doc
Summary:        Documentation for Django Allauth
Group:          Documentation
Requires:       %{name} = %{version}-%{release}

%description doc
This package contains the documentation for %{name}.
%endif # with_doc

%if 0%{?with_python3}
%package -n python%{python3_pkgversion}-%{pkgname}
Summary:        Social auth made simple
Group:          Development/Libraries

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-django
BuildRequires:  python%{python3_pkgversion}-mock >= 1.0.1
BuildRequires:  python%{python3_pkgversion}-requests
BuildRequires:  python%{python3_pkgversion}-requests-oauthlib
BuildRequires:  python%{python3_pkgversion}-openid

Requires:       python%{python3_pkgversion}-django >= 1.8
Requires:       python%{python3_pkgversion}-openid >= 3.0.8
Requires:       python%{python3_pkgversion}-requests
Requires:       python%{python3_pkgversion}-requests-oauthlib >= 0.3.0

%description -n python%{python3_pkgversion}-%{pkgname}
Integrated set of Django applications addressing authentication, registration,
account management as well as 3rd party (social) account authentication.

%if 0%{?with_doc}
%package -n python%{python3_pkgversion}-%{pkgname}-doc
Summary:        Documentation for Django Allauth
Group:          Documentation
Requires:       python%{python3_pkgversion}-%{pkgname} = %{version}-%{release}

%description -n python%{python3_pkgversion}-%{pkgname}-doc
This package contains the documentation for %{name}.
%endif # with_doc
%endif # with_python3


%prep
%setup -q -n %{pkgname}-%{version}

%if 0%{?with_python3}
cp -a . %{py3dir}
%endif # with_python3


%build
%{__python2} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif # with_python3

# build documentation
%if 0%{?with_doc}
pushd docs
    make html
popd
%endif # with_doc


%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root %{buildroot}
popd
%endif # with_python3


%check
%{__python2} %{_bindir}/django-admin test --pythonpath=. --settings=test_settings allauth
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} %{_bindir}/django-admin test --pythonpath=. --settings=test_settings allauth
%endif # with_python3


%files
%doc README.rst LICENSE AUTHORS
%{python2_sitelib}/allauth
%{python2_sitelib}/django_allauth-%{version}-py2.*.egg-info


%if 0%{?with_doc}
%files doc
%doc docs/_build/html
%endif # with_doc

%if 0%{?with_python3}
%files -n python%{python3_pkgversion}-%{pkgname}
%doc README.rst LICENSE AUTHORS
%{python3_sitelib}/allauth
%{python3_sitelib}/django_allauth-%{version}-py3.*.egg-info

%if 0%{?with_doc}
%files -n python%{python3_pkgversion}-%{pkgname}-doc
%doc docs/_build/html
%endif # with_doc
%endif # with_python3

%changelog
* Wed Feb 28 2018 Aurelien Bompard <abompard@fedoraproject.org> - 0.34.0-1
- version 0.34.0

* Fri Sep 02 2016 Aurelien Bompard <abompard@fedoraproject.org> - 0.27-1
- version 0.27.0

* Mon May 30 2016 Aurelien Bompard <abompard@fedoraproject.org> - 0.25.2-1
- Initial package
