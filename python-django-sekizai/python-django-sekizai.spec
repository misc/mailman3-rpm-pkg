%if 0%{?fedora} > 12 || 0%{?rhel} > 6
%global with_python3 1
%endif
# Until python3-django is packaged:
%global with_python3 0

%if 0%{?fedora}
%{!?python3_pkgversion: %global python3_pkgversion 3}
%else
%{!?python3_pkgversion: %global python3_pkgversion 34}
%endif

%global pypi_name django-sekizai

Name:           python-%{pypi_name}
Version:        0.9.0
Release:        1%{?dist}
Summary:        Django Template Blocks with extra functionality

License:        BSD
URL:            http://github.com/ojii/django-sekizai
Source0:        https://pypi.python.org/packages/d7/71/51edb4ae9ca16d3166f699ee267702f60e58af5e2a10c793ac6e0a787bd5/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch
 
BuildRequires:  python-setuptools
BuildRequires:  python2-devel
BuildRequires:  python-sphinx

Requires:       python-django-classy-tags >= 0.3.1

%description
Sekizai means "blocks" in Japanese, and that's what this app provides. A fresh
look at blocks. With django-sekizai you can define placeholders where your
blocks get rendered and at different places in your templates append to those
blocks. This is especially useful for css and javascript. Your sub-templates
can now define css and Javascript files to be included, and the css will be
nicely put at the top and the Javascript to the bottom, just like you should.
Also sekizai will ignore any duplicate content in a single block.

Please refer to the documentation in the docs/ directory or on RTFD for help:
http://django-sekizai.readthedocs.org/en/latest/


%if 0%{?with_python3}
%package -n     python%{python3_pkgversion}-%{pypi_name}
Summary:        Django Template Blocks with extra functionality
 
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-sphinx
 
Requires:       python%{python3_pkgversion}-django-classy-tags >= 0.3.1

%description -n python%{python3_pkgversion}-%{pypi_name}
Sekizai means "blocks" in Japanese, and that's what this app provides. A fresh
look at blocks. With django-sekizai you can define placeholders where your
blocks get rendered and at different places in your templates append to those
blocks. This is especially useful for css and javascript. Your sub-templates
can now define css and Javascript files to be included, and the css will be
nicely put at the top and the Javascript to the bottom, just like you should.
Also sekizai will ignore any duplicate content in a single block.

Please refer to the documentation in the docs/ directory or on RTFD for help:
http://django-sekizai.readthedocs.org/en/latest/
%endif


%package -n python-%{pypi_name}-doc
Summary:        django-sekizai documentation

%description -n python-%{pypi_name}-doc
Documentation for django-sekizai

%prep
%setup -q -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%if 0%{?with_python3}
cp -a . %{py3dir}
%endif # with_python3


%build
%{__python2} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif # with_python3

# generate html docs 
sphinx-build docs html
# remove the sphinx-build leftovers
rm -rf html/.{doctrees,buildinfo}

%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root %{buildroot}
popd
%endif # with_python3

%check
# https://github.com/ojii/django-sekizai/issues/59
#%%{__python2} runtests.py
#%%{__python3} runtests.py

%files
%doc README.rst LICENSE
%{python2_sitelib}/sekizai
%{python2_sitelib}/django_sekizai-%{version}-py?.?.egg-info

%if 0%{?with_python3}
%files -n python%{python3_pkgversion}-%{pypi_name}
%doc README.rst LICENSE
%{python3_sitelib}/sekizai
%{python3_sitelib}/django_sekizai-%{version}-py?.?.egg-info
%endif # with_python3

%files -n python-%{pypi_name}-doc
%doc html

%changelog
* Wed Jun 08 2016 Aurelien Bompard <abompard@fedoraproject.org> - 0.9.0-1
- Initial package.
