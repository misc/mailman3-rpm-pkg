These packages are made to help deploy a Mailman 3 infrastruture.

The main reasons for this repository are:
  - packages not in EL7 nor EPEL
  - version not appropriate in EL7 or EPEL
  - Python 3 support missing or not for the right version
    (some were 3.4-enabled but are now being rebuild slowly on 3.6)
  - packaging or upstream fix/adaptation needed

Patches are sent upstream when appropriate.

It is made to work with the following Ansible role:
  https://gitlab.com/osas/ansible-role-mailman3

