Name:           python-pylibmc
Version:        1.2.3
Release:        7%{?dist}
Summary:        Memcached client for Python

Group:          Development/Libraries
License:        BSD
URL:            https://sendapatch.se/projects/pylibmc/
Source0:        https://pypi.python.org/packages/source/p/pylibmc/pylibmc-%{version}.tar.gz

BuildRequires:  python-devel
BuildRequires:  python-setuptools
BuildRequires:  libmemcached-devel
BuildRequires:  zlib-devel

%description
pylibmc is a client in Python for memcached. It is a wrapper
around TangentOrg‘s libmemcached library. The interface is 
intentionally made as close to python-memcached as possible, 
so that applications can drop-in replace it.

%prep
%setup -q -n pylibmc-%{version}

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build
%{?filter_setup:
%filter_provides_in %{python_sitearch}/.*\.so$ 
%filter_setup
}

%install
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
chmod 755 $RPM_BUILD_ROOT%{python_sitearch}/_pylibmc.so

%files
%doc docs/ LICENSE README.rst
%{python_sitearch}/pylibmc-%{version}*.egg-info
%{python_sitearch}/pylibmc/
%{python_sitearch}/*.so


%changelog
* Fri Apr 19 2019 Marc Dequènes (Duck) <duck@redhat.com> - 1.2.3-7
- use HTTPS in source URL

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.3-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.3-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Tue Jan 28 2013 Rahul Sundaram <sundaram@fedoraproject.org> - 1.2.3-2
- use dist macro

* Tue Jan 28 2013 Rahul Sundaram <sundaram@fedoraproject.org> - 1.2.3-1
- upstream release 1.2.3
- resolves rhbz#905508

* Sat Sep 22 2012  Remi Collet <remi@fedoraproject.org> - 1.2.0-10.20110805gitf01c31
- rebuild against libmemcached.so.11 without SASL

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.0-9.20110805gitf01c31
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Mon Apr 23 2012  Remi Collet <remi@fedoraproject.org> - 1.2.0-8.20110805gitf01c31
- rebuild against libmemcached.so.10 with SASL

* Sun Apr 22 2012  Remi Collet <remi@fedoraproject.org> - 1.2.0-7.20110805gitf01c31
- rebuild against libmemcached.so.10
- add upstream patch to fix build

* Sat Mar 03 2012  Remi Collet <remi@fedoraproject.org> - 1.2.0-6.20110805gitf01c31
- rebuild against libmemcached.so.9

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.0-5.20110805gitf01c31
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Sat Sep 17 2011  Remi Collet <remi@fedoraproject.org> - 1.2.0-4.20110805gitf01c31
- rebuild against libmemcached.so.8

* Tue Aug 09 2011 Praveen Kumar <kumarpraveen.nitdgp@gmail.com> - 1.2.0-3.20110805gitf01c31
- Changed file pylibmc.so permission

* Tue Aug 09 2011 Praveen Kumar <kumarpraveen.nitdgp@gmail.com> - 1.2.0-2.20110805gitf01c31
- Added soname files

* Fri Aug 05 2011 Praveen Kumar <kumarpraveen.nitdgp@gmail.com> - 1.2.0-1.20110805gitf01c31
- Initial RPM release
