%if 0%{?fedora} > 12 || 0%{?rhel} > 6
%global with_python3 1
%endif
# Until python3-django is packaged:
%global with_python3 0

%if 0%{?fedora}
%{!?python3_pkgversion: %global python3_pkgversion 3}
%else
%{!?python3_pkgversion: %global python3_pkgversion 34}
%endif

%global pypi_name django-classy-tags

Name:           python-%{pypi_name}
Version:        0.7.2
Release:        1%{?dist}
Summary:        Class based template tags for Django

License:        BSD
URL:            http://github.com/ojii/django-classy-tags
Source0:        https://pypi.python.org/packages/1b/0f/9eaa1cc7a6d994f299e613fe0937aca3b4a8fd319625186e57e3704c3ff7/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch
 
BuildRequires:  python-setuptools
BuildRequires:  python2-devel
 
Requires:       python-django

%description
The goal of this project is to create a new way of writing Django template tags
which is fully compatible with the current Django templating infrastructure.
This new way should be easy, clean and require as little boilerplate code as
possible while still staying as powerful as possible.

Please refer to the documentation in the docs/ directory for help. For a HTML
rendered version of it please see http://django-classy-tags.rtfd.org.


%if 0%{?with_python3}
%package -n     python%{python3_pkgversion}-%{pypi_name}
Summary:        Class based template tags for Django

BuildRequires:  python3-pkgversion-macros
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-devel
 
Requires:       python%{python3_pkgversion}-django

%description -n python%{python3_pkgversion}-%{pypi_name}
The goal of this project is to create a new way of writing Django template tags
which is fully compatible with the current Django templating infrastructure.
This new way should be easy, clean and require as little boilerplate code as
possible while still staying as powerful as possible.

Please refer to the documentation in the docs/ directory for help. For a HTML
rendered version of it please see http://django-classy-tags.rtfd.org.
%endif # with_python3


%prep
%setup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%if 0%{?with_python3}
cp -a . %{py3dir}
%endif # with_python3


%build
%{__python2} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif # with_python3


%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root %{buildroot}
popd
%endif # with_python3


%check
# Not until runtests.py is included in the tarball
#%{__python2} runtests.py
#%if 0%{?with_python3}
#pushd %{py3dir}
#%{__python3} runtests.py
#popd
#%endif # with_python3


%files
%doc README.rst
%{python2_sitelib}/classytags
%{python2_sitelib}/django_classy_tags-%{version}-py?.?.egg-info

%if 0%{?with_python3}
%files -n python%{python3_pkgversion}-%{pypi_name} 
%doc README.rst
%{python3_sitelib}/classytags
%{python3_sitelib}/django_classy_tags-%{version}-py?.?.egg-info
%endif # with_python3


%changelog
* Wed Jun 08 2016 Aurelien Bompard <abompard@fedoraproject.org> - 
- Initial package.
