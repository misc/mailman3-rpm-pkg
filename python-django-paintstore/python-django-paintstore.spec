%global pypi_name django-paintstore

Name:           python-%{pypi_name}
Version:        0.2
Release:        1%{?dist}
Summary:        Integrates jQuery ColorPicker with the Django admin

License:        MIT
URL:            https://github.com/gsiegman/django-paintstore
Source0:        https://pypi.python.org/packages/source/d/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python-devel
BuildRequires:  python-setuptools


%description
django-paintstore is a Django app that integrates jQuery ColorPicker
with the Django admin.


%prep
%setup -q -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info


%build
%{__python} setup.py build


%install
%{__python} setup.py install --skip-build --root %{buildroot}


%files
%doc README.rst LICENSE
%{python_sitelib}/paintstore
%{python_sitelib}/django_paintstore-%{version}-py?.?.egg-info

%changelog
* Fri Apr 07 2017 Aurelien Bompard <abompard@fedoraproject.org> - 0.2-1
- Initial package.
