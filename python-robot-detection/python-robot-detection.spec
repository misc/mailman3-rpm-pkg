%global pypi_name robot-detection

Name:           python-%{pypi_name}
Version:        0.3
Release:        2%{?dist}
Summary:        Library for detecting if a HTTP User Agent header is likely to be a bot

License:        GPLv3+
URL:            https://www.celtic-knot-creator.com/robot-detection/
Source0:        https://pypi.python.org/packages/source/r/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python-devel
BuildRequires:  python-setuptools


%description
robot_detection is a python module to detect if a given HTTP
User Agent is a web crawler. It uses the list of registered robots from
http://www.robotstxt.org.

There is only one, function, ``is_robot`` that takes a string
(unicode or not) and returns True iff that string matches a known robot in the
robotstxt.org ...


%prep
%setup -q -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info



%build
%{__python} setup.py build


%install
%{__python} setup.py install --skip-build --root %{buildroot}


%files
%doc README LICENCE
%{python_sitelib}/robot_detection.py*
%{python_sitelib}/robot_detection-%{version}-py?.?.egg-info

%changelog
* Tue Apr 16 2019 Marc Dequènes (Duck) <duck@redhat.com> - 0.3-2
- use HTTPS un URLs

* Sat May 18 2013 Aurelien Bompard <abompard@fedoraproject.org> - 0.3-1
- Initial package.
