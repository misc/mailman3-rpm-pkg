
%global upstream_name mailman-hyperkitty
%global prerel 1

Name:           mailman3-hyperkitty
Version:        1.1.1
Release:        %{?prerel:0.}3%{?dist}
Summary:        Mailman archiver plugin for HyperKitty

License:        GPLv3
URL:            https://gitlab.com/mailman/%{upstream_name}
# version not available on pypi :-/
Source0:        mailman-hyperkitty-1.1.1.dev0.tar.gz
#Source0:        https://pypi.python.org/packages/source/m/%{upstream_name}/%{upstream_name}-%{version}%{?prerel:.dev0}.tar.gz
Patch0:         82dadd0b_fix_call_to__recover_backup_files.diff
Patch1:         84e05811_update_test_core.diff

BuildArch:      noarch

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-requests
BuildRequires:  python%{python3_pkgversion}-zope-interface
BuildRequires:  mailman3
BuildRequires:  python%{python3_pkgversion}-nose2
BuildRequires:  python%{python3_pkgversion}-mock

Requires:       mailman3
Requires:       python%{python3_pkgversion}-setuptools
Requires:       python%{python3_pkgversion}-requests
Requires:       python%{python3_pkgversion}-zope-interface


%description
This package contains a Mailman archiver plugin which sends emails to
HyperKitty, Mailman's web archiver.

All documentation on installing HyperKitty can be found in the documentation
provided by the HyperKitty package. It is also available online at the
following URL: http://hyperkitty.readthedocs.org.


%prep
%autosetup -n %{upstream_name}-%{version}%{?prerel:.dev0} -p 1


%build
%{__python3} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python3} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

# Mailman config file
install -D -m 644 mailman-hyperkitty.cfg \
    $RPM_BUILD_ROOT%{_sysconfdir}/mailman3.d/hyperkitty.cfg


%check
%{__python3} -m nose2


%files
%doc README.rst LICENSE.txt
%config %{_sysconfdir}/mailman3.d/hyperkitty.cfg
%{python3_sitelib}/*


%changelog
* Wed Apr 17 2019 Marc Dequènes (Duck) <duck@redhat.com> - 1.1.1-0.3
- add missing build-dependencies on Python nose2 and mock
- add patch to fix call to recover_backup_files in process_queue
- add patch to fix test to match recent mailman core

* Mon May 29 2017 Aurelien Bompard <abompard@fedoraproject.org> - 1.1.1-0.2
- git update to 1199be8

* Mon May 29 2017 Aurelien Bompard <abompard@fedoraproject.org> - 1.1.0-1
- version 1.1.0

* Wed Apr 29 2015 Aurelien Bompard <abompard@fedoraproject.org> - 1.0.0-1
- version 1.0.0

* Fri Mar 20 2015 Aurelien Bompard <abompard@fedoraproject.org> - 0.3
- initial package
