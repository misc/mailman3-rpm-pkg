%global upstream_name postorius
%global prerel 1

Name:           postorius
Version:        1.1.2
Release:        %{?prerel:0.}1%{?dist}.2
Summary:        Web user interface for GNU Mailman

License:        GPLv3
URL:            https://launchpad.net/postorius
Source0:        https://pypi.python.org/packages/source/p/%{upstream_name}/%{upstream_name}-%{version}%{?prerel:.dev0}.tar.gz
Source1:        postorius.apache.conf

# Patch settings to use the FHS
Patch0:         postorius-fhs.patch
Patch10:        postorius-feature-signals.patch
Patch11:        postorius-fix-depver.patch


BuildArch:      noarch

BuildRequires:  python-devel
BuildRequires:  python-sphinx
BuildRequires:  python-mailman-client
BuildRequires:  python-django >= 1.8.8-3
BuildRequires:  python-django < 1.9
# for the unit tests
BuildRequires:  python-mock
BuildRequires:  postfix
#BuildRequires:  python-django-nose
BuildRequires:  python-vcrpy
BuildRequires:  python-beautifulsoup4
BuildRequires:  python-django-mailman3

Requires:       python-mailman-client
Requires:       python-django >= 1.8.8-3
Requires:       python-django < 1.9
Requires:       python-django-mailman3


%description
The Postorius Django app provides a web user interface to access GNU Mailman.

%prep
%autosetup -p1 -n %{upstream_name}-%{version}%{?prerel:.dev0}

# Remove bundled egg-info
rm -rf src/%{upstream_name}.egg-info
# remove shebang on manage.py
sed -i -e '1d' example_project/manage.py
# remove executable permissions on wsgi.py
chmod -x example_project/wsgi.py
# remove __init__.py in example_project to prevent it from being
# installed (find_package won't find it). It's empty anyway.
#rm -f example_project/__init__.py


%build
%{__python} setup.py build

# generate html docs
sphinx-build -D html_theme='default' src/postorius/doc html
# remove the sphinx-build leftovers
rm -rf html/.{doctrees,buildinfo}


%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

# Install the Django files
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/sites/default
cp -p example_project/{manage,settings,test_settings,urls,wsgi}.py \
    %{buildroot}%{_sysconfdir}/%{name}/sites/default/
touch --reference example_project/manage.py \
    %{buildroot}%{_sysconfdir}/%{name}/sites/default/__init__.py
# Apache HTTPd config file
install -p -D -m 644 %{SOURCE1} %{buildroot}/%{_sysconfdir}/httpd/conf.d/postorius.conf
# Localstate directory
mkdir -p %{buildroot}%{_localstatedir}/lib/%{name}/sites/default/static
mkdir -p %{buildroot}%{_localstatedir}/lib/%{name}/sites/default/db
# Logs
mkdir -p %{buildroot}%{_localstatedir}/log/%{name}/
# Remove the doc from the site-packages dir
rm -rf %{python_sitelib}/%{name}/doc


%check
PYTHONPATH=`pwd`:`pwd`/src %{__python} example_project/manage.py test --settings=test_settings postorius


%post
# Build the static files cache
%{_bindir}/django-admin collectstatic \
    --pythonpath=%{_sysconfdir}/%{name}/sites/default \
    --settings=settings --noinput >/dev/null || :


%preun
# The static files are a cache and can be removed with the package
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    rm -rf %{_localstatedir}/lib/%{name}/sites/default/static
fi


%files
%doc html README.rst COPYING
%config(noreplace) %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/httpd/conf.d/postorius.conf
%{python_sitelib}/%{name}
%{python_sitelib}/%{upstream_name}-*-py?.?.egg-info
%dir %{_localstatedir}/lib/%{name}
%dir %{_localstatedir}/lib/%{name}/sites
%dir %{_localstatedir}/lib/%{name}/sites/default
%dir %{_localstatedir}/lib/%{name}/sites/default/static
%attr(755,apache,apache) %{_localstatedir}/lib/%{name}/sites/default/db
%attr(755,apache,apache) %{_localstatedir}/log/%{name}/


%changelog
* Fri Apr 19 2019 Marc Dequènes (Duck) <duck@redhat.com> - 1.1.1-0.3.2
- adapt dependency on python-django to be sure to get the right version

* Wed Feb 28 2018 Aurelien Bompard <abompard@fedoraproject.org> - 1.1.1-0.3.1
- git update to e3e087f

* Fri Nov 17 2017 Aurelien Bompard <abompard@fedoraproject.org> - 1.1.1-0.3
- git update to c6c3a3e

* Mon May 29 2017 Aurelien Bompard <abompard@fedoraproject.org> - 1.1.1-0.1
- version 1.1.0

* Wed Apr 29 2015 Aurelien Bompard <abompard@fedoraproject.org> - 1.0.1-1
- version 1.0.1

* Wed Jul 24 2013 Aurelien Bompard <abompard@fedoraproject.org> - 1.0.0-0.5.bzr147
- update to BZR rev 147
- drop patch 0

* Wed Mar 27 2013 Aurelien Bompard <abompard@fedoraproject.org> - 1.0.0-0.1.a1
- initial package
