%global pypi_name csscompressor

Name:           python-%{pypi_name}
Version:        0.9.4
Release:        2%{?dist}
Summary:        A python port of YUI CSS Compressor

License:        BSD
URL:            http://github.com/sprymix/csscompressor
Source0:        https://pypi.python.org/packages/dd/0c/7465590606effb750210225d845e816541bad7913fbbd6d200b6a0854d20/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch
 
BuildRequires:  python-setuptools
BuildRequires:  python2-devel
BuildRequires:  pytest

%description
Almost exact port of YUI CSS Compressor. Passes all original unittests.
You can use it in Python or in command line:
$ python -m csscompressor --help


%package -n     python%{python3_pkgversion}-%{pypi_name}
Summary:        A python port of YUI CSS Compressor
 
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-pytest

%description -n python%{python3_pkgversion}-%{pypi_name}
Almost exact port of YUI CSS Compressor. Passes all original unittests.
You can use it in Python or in command line:
$ python -m csscompressor --help


%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py2_build
%py3_build

%install
# Must do the subpackages' install first because the scripts in /usr/bin are
# overwritten with every setup.py install.
%py3_install

%py2_install

%check
py.test %{pypi_name}
py.test-3 %{pypi_name}


%files
%doc README.rst LICENSE
%{python2_sitelib}/%{pypi_name}
%{python2_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%files -n python%{python3_pkgversion}-%{pypi_name}
%doc README.rst LICENSE
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%changelog
* Tue Apr 16 2019 Marc Dequènes (Duck) <duck@redhat.com> - 0.9.4-2
- adapt to recent Python 3 packaging style

* Wed Jun 08 2016 Aurelien Bompard <abompard@fedoraproject.org> - 0.9.4-1
- Initial package.
