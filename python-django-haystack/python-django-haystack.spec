%global pypi_name django-haystack

Name:           python-%{pypi_name}
Version:        2.5.0
Release:        1%{?dist}
Summary:        Pluggable search for Django.

License:        BSD
URL:            http://haystacksearch.org/
Source0:        https://pypi.python.org/packages/6c/4f/86e39c8760048f0b75d99726c5448bc4114eaff2897920367aa6c72b9830/%{pypi_name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python2-devel
BuildRequires:  python-sphinx
BuildRequires:  python-setuptools
BuildRequires:  python-django

Requires:       python-django


%description
Haystack provides modular search for Django. It features a unified, familiar
API that allows you to plug in different search backends (such as Solr,
Elasticsearch, Whoosh, Xapian, etc.) without having to modify your code.

Haystack is BSD licensed, plays nicely with third-party app without needing to
modify the source and supports advanced features like faceting, More Like This,
highlighting, spatial search and spelling suggestions.

You can find more information at http://haystacksearch.org/.

%package docs
Summary: Documentation for Django Haystack pluggable search
# Not requiring the main package, as users may wish to install
# the documentation separately.

%description docs
Documentation for Django Haystack pluggable search

Haystack provides modular search for Django. It features a unified, familiar
API that allows you to plug in different search backends (such as Solr,
Elasticsearch, Whoosh, Xapian, etc.) without having to modify your code.

Haystack is BSD licensed, plays nicely with third-party app without needing to
modify the source and supports advanced features like faceting, More Like This,
highlighting, spatial search and spelling suggestions.

You can find more information at http://haystacksearch.org/.



%prep
%setup -q -n %{pypi_name}-%{version}



%build
%{__python} setup.py build

# Re-generate documentation
# Docs cannot be built in parallel
# We cannot build 'linkcheck' because it requires network access

pushd docs
make clean html htmlhelp latex json pickle changes
popd

%install
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

# Remove several useless files from the sources
find . -name ".gitignore" -exec rm -f {} \;
find . -name ".buildinfo" -exec rm -f {} \;
find . -name ".DS_Store" -exec rm -f {} \;
find . -name "last_build" -exec rm -f {} \;

%files
%doc LICENSE PKG-INFO README.rst AUTHORS
# For noarch packages: sitelib
%{python_sitelib}/haystack
%{python_sitelib}/django_haystack-%{version}*.egg-info/

%files docs
%doc docs/

%changelog
* Mon Aug 08 2016 Aurelien Bompard <abompard@fedoraproject.org> - 2.5.0-1
- version 2.5.0

* Tue Jan 19 2016 Aurelien Bompard <abompard@fedoraproject.org> - 2.4.1-1
- version 2.4.1

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Thu Feb 27 2014 Stephen Gallagher <sgallagh@redhat.com> 2.1.0-4
- Build -docs subpackage
- Remove hidden files and cruft from other OSes
- Force rebuild of documentation

* Fri Jan 24 2014 Stephen Gallagher <sgallagh@redhat.com> 2.1.0-3
- Remove extra BuildRequires: python-django

* Thu Jan 23 2014 Stephen Gallagher <sgallagh@redhat.com> - 2.1.0-2
- Correct %%description

* Thu Jan 23 2014 Stephen Gallagher <sgallagh@redhat.com> - 2.1.0-1
- Initial release
