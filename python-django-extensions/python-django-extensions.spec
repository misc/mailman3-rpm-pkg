%global pypi_name django-extensions

Name:           python-%{pypi_name}
Version:        1.6.1
Release:        3%{?dist}
Summary:        Extensions for Django

License:        BSD
URL:            https://github.com/django-extensions/django-extensions
Source0:        https://pypi.python.org/packages/source/d/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python2-devel
BuildRequires:  python-six >= 1.2

%if 0%{?fedora} >= 18 || 0%{?rhel} >= 7
Requires:       python-django
%else
Requires:       Django
%endif
Requires: python-openid
Requires: python-six >= 1.2

%description
Django Extensions is a collection of custom extensions for the Django Framework.
These include management commands, additional database fields,
admin extensions and much more.

%prep
%setup -q -n %{pypi_name}-%{version}

%build
%{__python} setup.py build

# get rid of emtpy files (.tmpl templates)
rm -r build/lib/django_extensions/conf

%install
%{__python} setup.py install --skip-build --root %{buildroot}
%find_lang django-extensions --all-name

%files -f django-extensions.lang
%doc LICENSE
%doc docs
%{python_sitelib}/django_extensions
%{python_sitelib}/django_extensions-%{version}-py?.?.egg-info

%attr(755,-,-) %{python_sitelib}/django_extensions/utils/dia2django.py
%attr(755,-,-) %{python_sitelib}/django_extensions/management/modelviz.py
%attr(755,-,-) %{python_sitelib}/django_extensions/management/commands/dumpscript.py
%attr(755,-,-) %{python_sitelib}/django_extensions/management/commands/pipchecker.py

%changelog
* Tue Apr 16 2019 Marc Dequènes (Duck) <duck@redhat.com> - 1.3.7-3
- use HTTPS in source URL

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Thu May 29 2014 Richard Marko <rmarko@fedoraproject.org> - 1.3.7-1
- New version

* Thu Oct 24 2013 Richard Marko <rmarko@fedoraproject.org> - 1.2.5-1
- New version

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Apr 18 2013 Richard Marko <rmarko@fedoraproject.org> - 1.1.1-1
- New version

* Fri Feb 15 2013 Richard Marko <rmarko@fedoraproject.org> - 1.0.3-2
- Remove empty tests/urls.py file.

* Tue Jan 29 2013 Richard Marko <rmarko@fedoraproject.org> - 1.0.3-1
- Initial package.
