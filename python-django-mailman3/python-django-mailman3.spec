%global with_python3 0

# EPEL7 support: default Python is Python 2, and Python2 packages prefix
# is unversioned.
%if 0%{?rhel} && 0%{?rhel} <= 7
%global py2_namespace python
%global default_pyver 2
%else
%global py2_namespace python2
%global default_pyver 3
%endif

%global pkgname django-mailman3
%global srcname django_mailman3
%global summary Django library to help interaction with Mailman
%global _description \
This package contains libraries and templates for \
Django-based interfaces interacting with Mailman. \
\
To use this application, add it to the `INSTALLED_APPS` list \
in the settings file of your Django server.

%global prerel 1

Name:		python-%{pkgname}
Version:	1.1.1
Release:	%{?prerel:0.}3%{?dist}
Summary:    %{summary}

License:	GPLv3+
URL:		https://gitlab.com/mailman/django-mailman3
# version not available on pypi :-/
Source0:        django-mailman3-1.1.1.dev0.tar.gz
#Source0:	https://pypi.python.org/packages/source/d/%{pkgname}/%{pkgname}-%{version}%{?prerel:.dev0}.tar.gz

BuildArch:      noarch

BuildRequires:  %{py2_namespace}-devel
BuildRequires:  %{py2_namespace}-setuptools
BuildRequires:  %{py2_namespace}-django
BuildRequires:  %{py2_namespace}-mock
BuildRequires:  %{py2_namespace}-mailman-client
BuildRequires:  %{py2_namespace}-django-allauth
BuildRequires:  %{py2_namespace}-django-gravatar2
BuildRequires:  pytz

%{?python_provide:%python_provide %{py2_namespace}-%{pkgname}}
Requires:       %{py2_namespace}-django >= 1.8
Requires:       %{py2_namespace}-mailman-client
Requires:       %{py2_namespace}-django-allauth
Requires:       %{py2_namespace}-django-gravatar2
Requires:       pytz

%description %{_description}


%if %{py2_namespace} != python
%package -n     %{py2_namespace}-%{pkgname}
Summary:        %{summary}
%{?python_provide:%python_provide %{py2_namespace}-%{pkgname}}

Requires:       %{py2_namespace}-django >= 1.8
Requires:       %{py2_namespace}-mailman-client
Requires:       %{py2_namespace}-django-allauth
Requires:       %{py2_namespace}-django-gravatar2
Requires:       pytz

%description -n %{py2_namespace}-%{pkgname} %{_description}
%endif # py2_namespace != python


%if 0%{?with_python3}
%package -n     python%{python3_pkgversion}-%{pkgname}
Summary:        %{summary}

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-django >= 1.8
BuildRequires:  python%{python3_pkgversion}-mock
BuildRequires:  python%{python3_pkgversion}-mailman-client
BuildRequires:  python%{python3_pkgversion}-django-allauth

%{?python_provide:%python_provide python%{python3_pkgversion}-%{pkgname}}
Requires:       python%{python3_pkgversion}-django >= 1.8
Requires:       python%{python3_pkgversion}-openid >= 3.0.8
Requires:       python%{python3_pkgversion}-requests >= 1.0.3
Requires:       python%{python3_pkgversion}-requests-oauthlib >= 0.3.0

%description -n python%{python3_pkgversion}-%{pkgname} %{_description}


%if 0%{?with_python3_other}
%package -n     python%{python3_other_pkgversion}-%{pkgname}
Summary:        %{summary}

BuildRequires:  python%{python3_other_pkgversion}-devel
BuildRequires:  python%{python3_other_pkgversion}-setuptools
BuildRequires:  python%{python3_other_pkgversion}-django >= 1.8
BuildRequires:  python%{python3_other_pkgversion}-mock
BuildRequires:  python%{python3_other_pkgversion}-mailman-client
BuildRequires:  python%{python3_other_pkgversion}-django-allauth

%{?python_provide:%python_provide python%{python3_other_pkgversion}-%{pkgname}}
Requires:       python%{python3_other_pkgversion}-django >= 1.8
Requires:       python%{python3_other_pkgversion}-openid >= 3.0.8
Requires:       python%{python3_other_pkgversion}-requests >= 1.0.3
Requires:       python%{python3_other_pkgversion}-requests-oauthlib >= 0.3.0

%description -n python%{python3_other_pkgversion}-%{pkgname} %{_description}
%endif # with_python3_other
%endif # with_python3


%prep
%setup -q -n %{pkgname}-%{version}%{?prerel:.dev0}


%build
%py2_build
%if 0%{?with_python3}
%py3_build
%if 0%{?with_python3_other}
%py3_other_build
%endif
%endif # with_python3


%install
%py2_install
%if 0%{?with_python3}
%py3_install
%if 0%{?with_python3_other}
%py3_other_install
%endif
%endif # with_python3


%check
%{__python2} %{_bindir}/django-admin test --pythonpath=. --settings=django_mailman3.tests.settings_test django_mailman3
%if 0%{?with_python3}
for python in %{__python3} %{__python3_other}; do
    ${python} %{_bindir}/django-admin test --pythonpath=. --settings=django_mailman3.tests.settings_test django_mailman3
done
%endif # with_python3


%files
%license COPYING.txt
%doc README.rst
%{python2_sitelib}/django_mailman3
%{python2_sitelib}/django_mailman3-*.egg-info

%if 0%{?with_python3}
%files -n python%{python3_pkgversion}-%{pkgname}
%license COPYING.txt
%doc README.rst
%{python3_sitelib}/django_mailman3
%{python3_sitelib}/django_mailman3-*.egg-info

%if 0%{?with_python3_other}
%files -n python%{python3_other_pkgversion}-%{pkgname}
%license COPYING.txt
%doc README.rst
%{python3_other_sitelib}/django_mailman3
%{python3_other_sitelib}/django_mailman3-*.egg-info
%endif # with_python3_other
%endif # with_python3


%changelog
* Sat Nov 18 2017 Aurelien Bompard <abompard@fedoraproject.org> - 1.1.1-0.3
- git update to 9c6a83d

* Mon May 29 2017 Aurelien Bompard <abompard@fedoraproject.org> - 1.1.1-0.1
- version 1.1.0
- modernize spec file

* Mon Aug 08 2016 Aurelien Bompard <abompard@fedoraproject.org> - 1.0.0-1
- initial package

