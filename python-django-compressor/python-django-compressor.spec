%if 0%{?fedora} > 12
%global with_python3 1
%else
%global with_python3 0
%endif
# Until python3-django is packaged:
%global with_python3 0

%if 0%{?fedora}
%{!?python3_pkgversion: %global python3_pkgversion 3}
%else
%{!?python3_pkgversion: %global python3_pkgversion 34}
%endif

%global pypi_name django_compressor

Name:           python-django-compressor
Version:        2.0
Release:        2%{?dist}
Summary:        Compresses linked and inline JavaScript or CSS into single cached files

License:        MIT
URL:            https://pypi.python.org/pypi/django_compressor/%{version}
Source0:        https://pypi.python.org/packages/source/d/%{pypi_name}/%{pypi_name}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python2-devel
BuildRequires:  python-setuptools
# Tests
BuildRequires:  python-django >= 1.8.8-3
BuildRequires:  python-django < 1.9
BuildRequires:  python-django-appconf
BuildRequires:  python-html5lib
BuildRequires:  python-mock
BuildRequires:  python-jinja2
BuildRequires:  python-lxml
BuildRequires:  python-beautifulsoup4
BuildRequires:  python-django-sekizai
BuildRequires:  python-csscompressor
BuildRequires:  python-rcssmin
BuildRequires:  python-rjsmin

Requires:       python-django-appconf >= 0.4
Requires:       python-versiontools
Requires:       python-django >= 1.8.8-3
Requires:       python-django < 1.9
Requires:       python-rcssmin
Requires:       python-rjsmin

%description
Django Compressor combines and compresses linked and inline Javascript
or CSS in a Django templates into cacheable static files by using the
compress template tag.  HTML in between {% compress js/css %} and
{% endcompress %} is parsed and searched for CSS or JS. These styles and
scripts are subsequently processed with optional, configurable compilers and
filters.

%if 0%{?with_python3}
%package -n python%{python3_pkgversion}-django-compressor
Summary:     Compresses linked and inline JavaScript or CSS into single cached files
Requires:    python%{python3_pkgversion}-versiontools
Requires:    python%{python3_pkgversion}-django-appconf
Requires:    python%{python3_pkgversion}-django >= 1.8.8-3
Requires:    python%{python3_pkgversion}-django < 1.9
Requires:    python%{python3_pkgversion}-rcssmin
Requires:    python%{python3_pkgversion}-rjsmin

BuildRequires: python3-pkgversion-macros
BuildRequires: python%{python3_pkgversion}-devel
BuildRequires: python%{python3_pkgversion}-setuptools
BuildRequires: python%{python3_pkgversion}-django >= 1.8.8-3
BuildRequires: python%{python3_pkgversion}-django < 1.9
BuildRequires: python%{python3_pkgversion}-django-appconf
BuildRequires: python%{python3_pkgversion}-rcssmin
BuildRequires: %{python3_pkgversion}ython3-rjsmin

%description -n python%{python3_pkgversion}-django-compressor
Django Compressor combines and compresses linked and inline Javascript
or CSS in a Django templates into cacheable static files by using the
compress template tag.  HTML in between {% compress js/css %} and
{% endcompress %} is parsed and searched for CSS or JS. These styles and
scripts are subsequently processed with optional, configurable compilers and
filters.
%endif


%prep
%setup -qc
mv %{pypi_name}-%{version} python2
pushd python2

# copy license etc to top level dir
cp -a LICENSE ..
cp -a README.rst ..
popd

%if 0%{?with_python3}
cp -a python2 python3
%endif

%build
pushd python2
%{__python2} setup.py build
popd

%if 0%{?with_python3}
pushd python3
%{__python3} setup.py build
popd
%endif

%install
pushd python2
%{__python2} setup.py install --skip-build --root %{buildroot}
popd

%if 0%{?with_python3}
pushd python3
%{__python3} setup.py install --skip-build --root %{buildroot}
popd
%endif

%check
pushd python2
%{__python2} %{_bindir}/django-admin test --pythonpath=. --settings=compressor.test_settings compressor
popd
%if 0%{?with_python3}
pushd python3
%{__python3} %{_bindir}/django-admin test --pythonpath=. --settings=compressor.test_settings compressor
popd
%endif


%files
%doc README.rst
%license LICENSE
%{python2_sitelib}/compressor
%{python2_sitelib}/%{pypi_name}-%{version}-py%{python2_version}.egg-info

%if 0%{?with_python3}
%files -n python%{python3_pkgversion}-django-compressor
%doc README.rst
%license LICENSE
%{python3_sitelib}/compressor
%{python3_sitelib}/%{pypi_name}-%{version}-py%{python3_version}.egg-info
%endif


%changelog
* Tue Apr 16 2019 Marc Dequènes (Duck) <duck@redhat.com> - 2.0-2
- use HTTPS in source URL
- adapt dependency on python-django to be sure to get the right version

* Wed Jun 08 2016 Aurelien Bompard <abompard@fedoraproject.org> - 2.0-1
- version 2.0

* Fri Nov 20 2015 Matthias Runge <mrunge@redhat.com> - 1.6-1
- update to 1.6 (rhbz#1283807)

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.5-3
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Thu Aug 27 2015 Matthias Runge <mrunge@redhat.com> - 1.5-2
- add python3 subpackage

* Wed Aug 26 2015 Matthias Runge <mrunge@redhat.com> - 1.5-1
- update to 1.5

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed Apr 15 2015 Matthias Runge <mrunge@redhat.com> - 1.4-3
- make compress command work on django-1.8

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Mon May 26 2014 Matthias Runge <mrunge@redhat.com> - 1.4-1
- update to 1.4 (rhbz#1100732)

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Wed Mar 20 2013 Matthias Runge <mrunge@redhat.com> - 1.3-1
- update to python-django-compressor-1.3 (rhbz#923735)

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Mon Sep 24 2012 Matthias Runge <mrunge@redhat.com> - 1.2-3
- add requirement Django/python-django

* Fri Sep 14 2012 Matthias Runge <mrunge@redhat.com> - 1.2-2
- add requirement python-versiontools

* Tue Sep 11 2012 Matthias Runge <mrunge@redhat.com> - 1.2-1
- Initial package.
